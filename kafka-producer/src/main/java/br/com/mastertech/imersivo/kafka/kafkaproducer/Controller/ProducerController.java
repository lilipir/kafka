package br.com.mastertech.imersivo.kafka.kafkaproducer.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.mastertech.imersivo.kafka.kafkaproducer.service.KafkaProducerService;
import br.com.mastertech.imersivo.kafka.model.Beirute;

@RestController
public class ProducerController {
	
	@Autowired
	private KafkaProducerService producerService;
	
	@PostMapping("/beirute")
	private void beirutear(@RequestBody Beirute beirute) {
		producerService.enviarBeirute(beirute);
	}

}
